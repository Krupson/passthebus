package service

import model.api.DataInsertResponse
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface ApiService {

    @FormUrlEncoded
    @POST("v1/insertData.php")
    fun insertData(
        @Field("key") key: String,
        @Field("signature") signature: String,
        @Field("passengers") passengers: Int
    ): Call<DataInsertResponse>
}