package service

import model.api.CameraResponse
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface CameraService {

    @POST("statistic")
    fun readCameraData(): Call<CameraResponse>

    @FormUrlEncoded
    @POST("ResetZone")
    fun resetCounter(@Field("Id") id: Int): Call<Any>
}