package util

import com.google.gson.Gson
import model.config.Config

object ConfigUtil {

    val config: Config

    init {
        val plainConfig = ConfigUtil::class.java.getResource("/config.json")?.readText()
        if (plainConfig != null) {
            config = Gson().fromJson(plainConfig, Config::class.java)
        } else {
            throw IllegalStateException("Error reading config file")
        }
    }
}