package model.config

data class Camera(
    val name: String,
    val url: String
)