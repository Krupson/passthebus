package model.config

data class Config(
    val vehicleId: Int,
    val cameras: List<Camera>,
    val publicKey: String,
    val privateKey: String,
    val api: String,
    val fetchingDataInterval: Long
)