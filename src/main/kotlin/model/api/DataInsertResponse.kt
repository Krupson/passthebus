package model.api

data class DataInsertResponse(
    val resetCounterFlag: Boolean
)