package model.api

import com.google.gson.annotations.SerializedName

data class CameraResponse(

    @SerializedName("0-in")
    private val inCountString: String,

    @SerializedName("0-out")
    private val outCountString: String
) {

    val inCount: Int
        get() = inCountString.toIntOrNull() ?: 0

    val outCount: Int
        get() = outCountString.toIntOrNull() ?: 0

    val diff: Int
        get() = inCount - outCount
}