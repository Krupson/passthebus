import factory.RetrofitServiceFactory
import service.CameraService
import util.ConfigUtil
import util.HashUtil
import java.time.LocalDateTime
import kotlin.concurrent.fixedRateTimer

fun main() {
    val cameras = mutableMapOf<String, CameraService>().apply {
        ConfigUtil.config.cameras.forEach { camera ->
            log("Creating \"${camera.name}\" camera service")
            this[camera.name] = RetrofitServiceFactory.createCameraService(camera.url)
        }
    }.toMap()
    log("Camera services ready")

    fixedRateTimer("data-grabber", false, 0L, ConfigUtil.config.fetchingDataInterval * 1000) {
        println()
        var passengersCount = 0
        var errorTick = false
        cameras.forEach fetchingData@{ name, service ->
            log("Fetching data from camera \"$name\"")
            service.readCameraData()
                .execute()
                .let { response ->
                    if (response.isSuccessful) {
                        response.body()?.let { cameraData ->
                            val cameraSummary = cameraData.diff
                            log("Camera \"$name\" summary: $cameraSummary")
                            passengersCount += cameraSummary
                        } ?: run {
                            log("Error while fetching data from camera \"$name\" [null body], aborting...")
                            errorTick = true
                            return@fetchingData
                        }
                    } else {
                        log("Error while fetching data from camera \"$name\" [HTTP ${response.code()}], aborting...")
                        errorTick = true
                        return@fetchingData
                    }
                }
        }
        if (!errorTick) {
            log("Successfully fetched data form all cameras, current passengers count: $passengersCount")
            log("Sending result to database")

            val signatureBuilder = StringBuilder(ConfigUtil.config.privateKey).apply {
                append("-")
                append(ConfigUtil.config.vehicleId)
                append("-")
                append(passengersCount)
            }
            val signature = HashUtil.sha1(signatureBuilder.toString())

            RetrofitServiceFactory.apiService.insertData(
                key = ConfigUtil.config.publicKey,
                signature = signature,
                passengers = passengersCount
            ).execute()
                .let { response ->
                    if (response.isSuccessful) {
                        log("Data successfully uploaded to database")
                        val shouldResetCounter = response.body()?.resetCounterFlag ?: false
                        if(shouldResetCounter) {
                            cameras.forEach { name, service ->
                                log("Resetting camera \"$name\" counter", false)
                                service.resetCounter(1).execute().let { camResetResponse ->
                                    if (camResetResponse.isSuccessful) {
                                        println("OK")
                                    } else {
                                        println("Failed")
                                    }
                                }
                            }
                        }
                    } else {
                        log("Error while uploading results to database [HTTP ${response.code()}]")
                    }
                }
        }
    }
}

fun log(message: String, newLine: Boolean = true) {
    val now = LocalDateTime.now()
    if(newLine) {
        println("[$now]: $message")
    } else {
        print("[$now]: $message: ")
    }
}