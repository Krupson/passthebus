package factory

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import service.ApiService
import service.CameraService
import util.ConfigUtil

object RetrofitServiceFactory {

    val apiService by lazy {
        Retrofit.Builder()
            .baseUrl(ConfigUtil.config.api)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(ApiService::class.java)
    }

    fun createCameraService(cameraUrl: String): CameraService {
        return Retrofit.Builder()
            .baseUrl(cameraUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(CameraService::class.java)
    }
}